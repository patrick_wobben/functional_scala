package wrangler

import java.io.{File, InputStream}

import common.xml.Xml
import http.HttpRequest
import mappers.{FileMapper, PathMapper}

import scala.xml.{Node, NodeSeq, XML}

class SimpleHtmlWrangler extends HtmlFilter(null)
{
  var directory: File = _
  var _output: String => Unit = _
  val hrefValidator: Node => Boolean = nodeHasAttribute("href", _)
  val srcValidator: Node => Boolean = nodeHasAttribute("src", _)
  val hrefStripper: Node => Unit = stripAttribute("href", _)
  val srcStripper: Node => Unit = stripAttribute("src", _)

  def wrangle(url: String):Unit =
  {
    var domain: String = PathMapper
      .from(url)
        .toDomain
          .getResult
    domain = domain
        .substring(domain.indexOf("//") + 2)
          .replaceAll("[.:]", "_")

    directory = FileMapper
        .from(s"resources/$domain/")
          .makeDirectory()

    HttpRequest
      .load(url)
      .onComplete(success, error)
  }

  def success(inputStream: InputStream):Unit =
  {
    Xml.read(inputStream)
        .map("a", wrangleLink)
        .map("h1", wrangleH1)
        .map("h2", wrangleH2)
        .map("img", wrangleImg)
  }

  def error(inputStream: InputStream): Unit =
  {
    if(inputStream != null)
    {
      val byteArray: Array[Byte] = new Array[Byte](10000)
      inputStream.read(byteArray)
      println(byteArray.toString)
    }
  }

  def stripAttribute(value: String, node: Node): Unit =
      _output(node
        .attribute(value)
          .get
            .toString
            .trim)

  def nodeHasAttribute(value: String, node: Node): Boolean =
      node
        .attribute(value)
          .isDefined

  def nodeIsNotEmpty(node: Node): Boolean =
      !node.text.isEmpty

  def stripText(node: Node): Unit =
      _output(node
        .text
        .trim)

  def wrangleLink(nodeSequence: NodeSeq):Unit =
      nodeSequence
        .filter(hrefValidator)
          .foreach(hrefStripper)

  def wrangleH1(nodeSequence: NodeSeq):Unit =
      nodeSequence
        .filter(nodeIsNotEmpty)
          .foreach(stripText)

  def wrangleH2(nodeSequence: NodeSeq):Unit =
      nodeSequence
        .filter(nodeIsNotEmpty)
          .foreach(stripText)

  def wrangleImg(nodeSequence: NodeSeq):Unit =
      nodeSequence
        .filter(srcValidator)
          .foreach(srcStripper)

  def readImage(node: Node): Unit =
      HttpRequest
        .load(node.attribute("src").get.toString)
        .onComplete(inputStream =>
          FileMapper.from(
            directory.getAbsolutePath + "/" + PathMapper
              .from(node.attribute("src").get.toString)
              .toName
              .getResult)
            .writeAll(inputStream)
          , error)

  def setOutput(output: String => Unit): Unit =
    _output = output

}
