package wrangler

import scala.xml.NodeSeq

abstract class HtmlFilter(map: Map[String, NodeSeq => Unit])
{
  var filterMap:Map[String, NodeSeq => Unit] = map
  def next(htmlFilter: HtmlFilter): this.type =
  {
    filterMap = htmlFilter.filterMap
    this
  }
}
