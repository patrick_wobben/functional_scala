package gui

import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File

import javax.imageio.ImageIO

import scala.swing.Panel

class ImagePanel(path: String = "") extends Panel{

  private var _imagePath = path
  private var bufferedImage:BufferedImage = _

  if(!_imagePath.isEmpty)
    bufferedImage = ImageIO.read(new File(_imagePath))

  def imagePath: String = _imagePath

  def imagePath_=(value:String)
  {
    _imagePath = value
    bufferedImage = ImageIO.read(new File(_imagePath))
  }

  override def paintComponent(g:Graphics2D): Unit =
    if (null != bufferedImage)
      g.drawImage(
        bufferedImage,
        0,
        0,
        preferredSize.width,
        preferredSize.height,
        null)
}
