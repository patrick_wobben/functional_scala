package gui

import scala.swing.{Action, Button, Color}

class HtmlCrawlerButton(title: String)(action: Action) extends Button (action)
{
  background = new Color(50, 50, 50)
}
