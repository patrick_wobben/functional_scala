package gui

import scala.swing.{BoxPanel, Button, Orientation}

class SideMenu extends BoxPanel(Orientation.Vertical)
{
  contents += Button("Crawl"){
    {
      visible = false
    }
  }
  contents += Button("Collection")(println("Yes"))
  contents += Button("Close")(sys.exit(0))
}
