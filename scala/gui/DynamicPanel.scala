package gui

import scala.swing.{BoxPanel, GridBagPanel, Orientation}

class DynamicPanel extends GridBagPanel
{
  def constraints(x: Int, y: Int,
                  gridWidth: Int = 1, gridHeight: Int = 1,
                  weightx: Double = 0.0, weighty: Double = 0.0,
                  fill: GridBagPanel.Fill.Value = GridBagPanel.Fill.None) : Constraints =
  {
    val c = new Constraints
    c.gridx = x
    c.gridy = y
    c.gridwidth = gridWidth
    c.gridheight = gridHeight
    c.weightx = weightx
    c.weighty = weighty
    c.fill = fill
    c
  }
}
