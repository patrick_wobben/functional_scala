package gui

import wrangler.SimpleHtmlWrangler

import scala.swing.{BoxPanel, Button, Color, Dimension, GridBagPanel, GridPanel, Label, MainFrame, Orientation, SplitPane}

class HtmlCrawlerUI(wrangler: SimpleHtmlWrangler) extends MainFrame{
  title = "HtmlCrawler"
  preferredSize = new Dimension(980, 720)
  contents = new BoxPanel(Orientation.Vertical)
  {
    contents += new DynamicPanel
    {
//      add(new SideMenu,
//        constraints(0, 0, weighty = 1.0, fill = GridBagPanel.Fill.Vertical))
      add(new CrawlerPanel(wrangler),
        constraints(0, 0, weightx = 1.0, weighty = 1.0, fill = GridBagPanel.Fill.Both))
    }
    background = new Color(50, 50, 50)
  }
}
