package gui

import wrangler.SimpleHtmlWrangler

import scala.swing.{Action, BoxPanel, Button, Dimension, GridBagPanel, GridPanel, Orientation, TextArea, TextField}
import scala.xml.{Node, NodeSeq}

class CrawlerPanel(wrangler: SimpleHtmlWrangler) extends BoxPanel(Orientation.Vertical)
{
  val urlTextfield: TextField = new TextField
  {
    maximumSize = new Dimension(700, 40)
  }
  val outputConsole: TextArea = new TextArea
  {
    maximumSize = new Dimension(700, 680)
    minimumSize = new Dimension(700, 680)
    preferredSize = new Dimension(700, 680)
    //    enabled = false
  }
  wrangler.setOutput(addToConsole)
  val urlButton: Button = new Button("Crawl")
  {
    minimumSize = new Dimension(280, 40)
    maximumSize = new Dimension(280, 40)
    preferredSize = new Dimension(280, 40)
    action = new Action("Crawl") {
      def apply() {
        outputConsole.text = ""
        wrangler.wrangle(urlTextfield.text)
      }
    }
  }

  contents += new BoxPanel(Orientation.Horizontal)
  {
    minimumSize = new Dimension(980, 40)
    contents += new BoxPanel(Orientation.Vertical)
    {
      minimumSize = new Dimension(700, 40)
      contents += urlTextfield
    }
    contents += new BoxPanel(Orientation.Vertical)
    {
      minimumSize = new Dimension(280, 40)
      contents += urlButton
    }
  }

  contents += new BoxPanel(Orientation.Horizontal)
  {
    minimumSize = new Dimension(980, 680)
    contents += new BoxPanel(Orientation.Vertical)
    {
      minimumSize = new Dimension(700, 680)
      contents += outputConsole
    }
    contents += new BoxPanel(Orientation.Vertical)
    {
      maximumSize = new Dimension(280, 680)
      minimumSize = new Dimension(280, 680)
      preferredSize = new Dimension(280, 680)
    }
  }

  def addToConsole(node: String):Unit =
    outputConsole.text += node + "\n"
}
