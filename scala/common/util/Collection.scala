package common.util

abstract class Collection[AnyType: Manifest]
{
  private var _capacity: Int  = 15
  protected var _size: Int    = 0
  protected var _internalArray: Array[AnyType] = new Array[AnyType](_capacity)

  def ensureCapacity():Unit =
  {
    if(_capacity + 1 < _internalArray.length)
      return

    val temp: Array[AnyType] = _internalArray
    _capacity = _capacity * 2 + 1
    _internalArray = new Array[AnyType](_capacity)
    Array.copy(temp, 0, _internalArray, 0, temp.length)
  }

  def get(index : Int):AnyType =
    _internalArray(index)

  def length: Int =
    _size
}
