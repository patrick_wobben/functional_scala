package common.util
import util.control.Breaks._

object MapperList
{
  def sort[AnyType <: Ordered[AnyType]](list : MapperList[AnyType]): Unit =
  {
    for(i <- 0 until list.length - 1)
      if(list.get(i) < list.get(i + 1))
        breakable{
          for(j <- i until 0 by -1)
            if(list.get(j - 1) < list.get(j))
            {
              val temp: AnyType = list.get(j)
              list.set(j, list.get(j - 1))
              list.set(j - 1, temp)
              break
            }
          }
  }
}

class MapperList[AnyType: Manifest] extends Collection[AnyType]
{
  def print(): Unit =
  {
    for(i <- 0 until _size)
      println(_internalArray(i).toString)
  }

  def set(index: Int, value: AnyType):Unit =
  {
    _internalArray(index) = value
  }

  def add(value : AnyType): Unit =
  {
    ensureCapacity()
    _internalArray(_size) = value
    _size += 1
  }

  override def toString: String =
  {
    _internalArray.toString
  }
}
