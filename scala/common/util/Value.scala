package common.util

class Value(value : Int) extends Ordered[Value]
{
  val _value: Int = value
  override def compare(that: Value): Int = _value - that._value

  override def toString: String =
  {
    _value + ""
  }
}
