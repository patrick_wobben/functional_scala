package common.thread

import mappers.Condition

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object TaskPool
{
  private val _taskCount: Int = 10
  private var _taskList: List[Task] = List.tabulate(_taskCount)(id => new Task(id))
  private var _taskAvailability: mutable.Queue[Int] = new mutable.Queue[Int]()    //Create queue of available Task
    .++(List.tabulate(_taskCount)(n => n))
  private var _taskBackList: ListBuffer[Nothing => Unit] = new ListBuffer[Nothing => Unit]


  def newTask(doFunc: => Unit):Task =
    storeTask(doFunc)
      .last()

  def storeTask(doFunc: => Unit):this.type =
  {
    if(_taskAvailability.nonEmpty)
      _taskList(_taskAvailability.dequeue())
        .set(doFunc)
        .start()
    else
      _taskBackList.+:(doFunc)

      println(_taskAvailability)
    this
  }

  def last(): Task =
    _taskList.last

  class Task(id: Int)
  {
    var _thread: Thread = new Thread()

    def set(doFunc: => Unit): this.type =
    {
      _thread = new Thread(() => {
        doFunc
        _taskAvailability.enqueue(id)
      })
//      Condition.pick(_thread)
//        .when(_thread => !_thread.isAlive,
//          () => {
//                _thread = new Thread(() => doFunc)
//              })
      this
    }

    def start(): Unit =
      _thread.start()
  }
}
