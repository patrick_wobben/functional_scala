package common.xml

import java.io.InputStream

import javax.xml.parsers.SAXParser
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl

import scala.xml.parsing.NoBindingFactoryAdapter
import scala.xml.{InputSource, Node}

object Xml
{
  val adapter: NoBindingFactoryAdapter = new NoBindingFactoryAdapter
  val parser: SAXParser = (new SAXFactoryImpl).newSAXParser

  def read(inputStream: InputStream): Reader =
    new Reader(inputStream)

  class Reader(inputStream: InputStream)
  {
    val content: Node = adapter
      .loadXML(new InputSource(inputStream), parser)

    def map(tag : String, mapFunc: Node => Unit): this.type =
    {
      (content \\ tag)
        .foreach(mapFunc)
      this
    }
  }
}