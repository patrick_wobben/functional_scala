import common.util.{Collection, MapperList, Value}
import debug.Performance
import examples._
import gui.HtmlCrawlerUI
import wrangler.SimpleHtmlWrangler

object Main extends App {
  println("Hello Happy World!")
//  new examples.TypeExamples().example()
//  new examples.IntegerExamples().example()
//  new examples.StringExamples().example()
//  new examples.ArrayExamples().example()
//  new FunctionExamples().example()
//  new FloatExamples().example()
//  new LoopExamples().example()
//  new RangeExamples().example()
//  new TupleExamples().example()
//    new SetExamples().example()
  //  new ListExamples().example()
//    new ClassExamples().example()

//  val list:MapperList[Value] = new MapperList[Value]()
//  for(i <- 10 to 0 by -1)
//    list.add(new Value(i))
//
//  list.print()
//  MapperList.sort(list)
//  list.print()

//  val test: List[Int] = List[Int](1, 2, 3, 4)
//  ObjectMapper.from(test).foreach(obj =>
//    obj.end(integer =>
//      println(integer)))
//

//  val htmlLogger: HtmlLogger = new HtmlLogger
  val htmlWrangler: SimpleHtmlWrangler = new SimpleHtmlWrangler
//
  val crawlerUserInterface = new HtmlCrawlerUI(htmlWrangler)
  crawlerUserInterface.visible = true
//

//  Performance.test(
//    Performance.printRuntime)
}
