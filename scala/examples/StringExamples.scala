package examples

class StringExamples extends Example {
  override def example() : Unit =
  {
    println('\n')
    println("String Examples")

    //#1 String
    val string: String = "abc"
    println(string)

    //#2 String length
    val stringLength: Int = "abc".length
    println(stringLength)

    //#3 Concatenation
    val concatenation: String = 4 + "abc" + 4
    println(concatenation)

    //#4 Multiline strings
    val multiline: String = """This is
                        a multiline
                        string."""
    println(multiline)
  }
}
