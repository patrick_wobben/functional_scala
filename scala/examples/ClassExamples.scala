package examples

class ClassExamples extends Example
{
  override def example(): Unit =
  {
    // #1 Returning immutable properties without ()... bless this...
    val person: Person = new Person("Patrick", "Wobben")
    println(person.immutablePublicFirstName)
    // person.immutablePublicFirstname = "Rick"   < No annoying "getThis()"
    //person.firstname = "Rick"
  }

  class Person(firstName: String)
  {
    def this(firstName: String, lastName: String) =
      this(firstName)

    def immutablePublicFirstName: String = firstName  // < Why can't Java do this
  }
}
