package examples

class TupleExamples extends Example
{
  override def example(): Unit =
  {
    // #1 Simple tuple
    println("#1 Simple tuple")
    val person = ("Patrick", "Wobben") // < Intellij doesn't recognize my surname
    println(person)

    // #2 Multivalue assignments
    val (x, y) = (1, 2)
    println(x, y)       // < That's actually pretty cool

    val z, w = 1
    print(z, w)
  }
}
