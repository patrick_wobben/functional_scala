package examples

class ArrayExamples extends Example
{
  override def example(): Unit =
  {
    println("#1 Creating array 1")
    val cArray1:Array[Int] = new Array[Int](1)
    println(cArray1.foreach(println))

    println("#2 Creating array 2")
    val cArray2 = Array[Int](3, 3, 3)
    println(cArray2.foreach(println))

    println("#3 Creating array 3")
    val cArray3 = Array(3, 3, 3)
    println(cArray3.foreach(println))

    println("#4 Print each element of array")
    Array(1, 2, 3)
      .foreach(println)

    println("#5 Get 2nd element")
    println(
      Array(4, 5, 6)(1))

    println("#6 Adding element to array")
    val cArray4 = Array(3, 3, 3)
    cArray4(1) = 5
    println(cArray4.foreach(println))
  }
}
