package examples

class SetExamples extends Example
{
  override def example(): Unit =
  {
    val fruit = Set("apple", "orange", "peach", "banana")
    val fruitSubset = Set("apple", "orange")
    val notFruitSubset = Set("apple", "potato")
    val moreFruit = Set("melon", "pear", "mango")
    val intersectingFruit = Set("apple", "orange", "mango")

    // #1 Basic set example
    println("#1 Basic set")
    println(fruit("peach"))
    println(fruit("potato"))
    println(fruit contains "peach")
    println(fruit.contains("potato"))

    // #2 Subset example
    println("\n#2 Subset set")
    println(fruitSubset subsetOf fruit)
    println(notFruitSubset subsetOf fruit)

    // #3 Single addition to set
    println("\n#3 Single addition to set")
    println(fruit + "pear")
    println(fruit)

    // #4 Multiple additions to set
    println("\n#4 Multiple additions to set")
    println(fruit + ("pear", "melon"))
    println(fruit)

    // #5 All elements
    println("\n#5 All elements")
    println(fruit ++ moreFruit)
    println(fruit)

    // #6 Single removal from set
    println("\n#6 Single removal from set")
    println(fruit - "apple")
    println(fruit)

    // #7 Multiple removed from set
    println("\n#7 Multiple removed from set")
    println(fruit - ("peach", "banana", "melon"))
    println(fruit)

    // #8 Remove elements from other set
    println("\n#8 Remove elements from other set")
    println(fruit -- fruitSubset)
    println(fruit)

    // #9 Empty set of same class
    println("\n#9 Empty set of same class")
    println(fruit.empty)
    println(fruit)

    // #10 Intersect set
    println("\n#10 Intersect set")
    println(fruit)
    println(intersectingFruit)
    println(fruit & intersectingFruit)
    println(fruit intersect fruitSubset)
    println(fruit.intersect(intersectingFruit))

    // #11 Union set
    println("\n#10 Union set")
    println(fruit)
    println(intersectingFruit)
    println(fruit | intersectingFruit)

    // #12 Difference set
    println("\n#12 Difference set")
    print(fruit)
    print(intersectingFruit)
    println(fruit &~ intersectingFruit)
    println(fruit diff intersectingFruit)

    // #13 Mutable set
    var mutableSet = Set("M", "u", "t", "a", "b", "l", "e")
    mutableSet += ("S", "e", "t")
    println(mutableSet)
    mutableSet -= ("S", "e", "t")
    println(mutableSet)

    /*
      Set's can't contain duplicates, lists can contain
      Array, List and Vectors are ordered
     */
  }
}
