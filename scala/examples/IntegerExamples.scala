package examples

class IntegerExamples extends Example {
  override def example(): Unit =
  {
    println('\n')
    println("Int Examples")

    //#1 Addition
    val addition: Int = 1 + 1
    println(s"#1 Addition:        1 + 1 = $addition")

    //#2 Subtraction
    val subtraction: Int = 2 - 1
    println(s"#2 Subtraction:     2 - 1 = $subtraction")

    //#3 Multiplication
    val multiplication: Int = 2 * 2
    println(s"#3 Multiplication:  2 * 2 = $multiplication")

    //#4 Division
    val division: Int = 4 / 2
    println(s"#4 Division:        4 / 2 = $division")

    //#5 To Float
    val toFloat: Float = 5.toFloat
    println(s"#5 To Float:        5.toFloat = $toFloat")
  }
}
