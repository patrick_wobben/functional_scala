package examples

class RangeExamples extends Example
{
  override def example(): Unit =
  {
    // #1 Simple range
    println("#1 Simple range")
    val range: Range = 0 until 10
    printRange(range)

    // #2 Simple reverse range
    println("\n#2 Simple reverse range")
    val reverseRange: Range = 10 to 0 by -1
    printRange(reverseRange)
  }

  def printRange(range: Range):Unit =
  {
    println(range)
    print("Range(")
    range.foreach(value =>
      if(value != range.last)
        print(value + ", ")
      else
        print(value))
    println(')')
    println("Start: " + range.start)
    println("End: " + range.end)
    println("Step size: " + range.step)
  }
}
