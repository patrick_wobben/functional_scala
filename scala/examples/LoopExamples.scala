package examples

class LoopExamples extends Example
{
  override def example(): Unit =
  {
    // #1 basic while loop
    println("#1 Basic while loop")
    var i = 0
    var string = ""
    while(i < 10)
    {
      string += i + " "
//    string += i + ' '     < Works with int values instead of sting
      i += 1
    }
    print(string)

    // #2 basic? for loop
    println("\n\n #2 Basic for loop")
    val printString = "Print this string one by one"
    for(i <- 0 until printString.length)
      print(printString(i))

    // #3 reverse for loop
    println("\n\n #3 Reverse for loop")
    val printString2 = "Print this string backwards"
    for(i <- (printString.length - 2) to 0 by -1)
      print(printString2(i))

    // #4 basic foreach loop
    println("\n\n #4 Basic foreach loop")
    val array = Array("test", "1", "923842", "2", "3")
    array
        .filter(str => str == "test" || str.toInt < 4)
          .foreach(print)
  }
}
