package examples

import java.util.Date

//noinspection TypeAnnotation,RemoveRedundantReturn
//noinspection RemoveRedundantReturn
class FunctionExamples extends Example {
  override def example(): Unit = {

    //#1 Java like function
    println(javaLikeFunction(10))

    //#2 Java like function without return
    println(javaLikeFunctionWithoutReturn(10))

    //#3 DotNet Functional function
    println(dotNetFunctionalLikeFunction(10))

    //#4 Function stripped from all its types except the parameter
    println(functionStrippedFromAllTypesExceptTheParameter(10))

    println(factorial(5))
    println(apply(encapsulateWithBrackets, 10))

    println(stringConcatenate("Hello")("Happy")("World"))
    println(stringCompare("Sad")("Happy"))

    //# Partially filled function
    val stringConcatenateAlias = stringConcatenate _
    val prePart = stringConcatenateAlias("Pre")
    val preMidPart = prePart("Mid")
    val preMidPostResult = preMidPart("Post")
    println(preMidPostResult)

    //# Anonymous Functions
    val increment = (value:Int) => value + 1
    println(increment(5) + increment(10))

    val multiply = (pre:Int, post:Int) => pre * post
    println(multiply(5, 10))

    //# Predefined Anonymous Functions
    val userDirectory = () => {System.getProperty("user.dir")}
    println(userDirectory())

    //# Partially bound function
    new Thread(() => {
      val date = new Date
      val logWithDateBound = log(date,_: String)
      logWithDateBound("Hello")
      Thread.sleep(2500)
      logWithDateBound("Happy")
      Thread.sleep(2500)
      logWithDateBound("World")
    }).start()
  }

  //#1 Java like function
  def javaLikeFunction(value: Int): Int = {
    return value + value
  }

  //#2 Java like function without return
  def javaLikeFunctionWithoutReturn(value: Int): Int = {
    value + value                             //Still returns value
  }

  //#3 DotNet Functional function
  def dotNetFunctionalLikeFunction(value: Int): Int =
    value + value

  //#4 Function stripped from all its types except the parameter
  def functionStrippedFromAllTypesExceptTheParameter(value : Int) =
    value + value

  //# Partially bound function
  def log(date: Date, message: String)=
    println(date + "----" + message)

  //# Nested functions
  def factorial(value: Int): Int = {
    def fact(value: Int, accumulator: Int): Int =
      if(value < 2)
        accumulator                           //Actually returns the value
      else
        fact(value - 1, value * accumulator)  //Actually returns the value
    fact(value, 1)
  }

  //# High-order functions
  def apply(applyFunction: Int => String, parameter: Int) =
    applyFunction(parameter)

  def encapsulateWithBrackets[AnyType](value: AnyType) =
    '[' + value.toString + ']'

  //# Currying functions #1
  def stringConcatenate(pre:String)(mid:String)(post:String) =
    pre + " " + mid + " " + post

  //# Currying functions #2
  def stringCompare(pre:String) = (post:String) => pre == post
}
