package examples

class TypeExamples extends Example {
  override def example(): Unit =
  {
    println('\n')
    println("Type Examples")

    val byte:Byte     = 127
    val short:Short   = 32767
    val int:Int       = 2147483647
    val long:Long     = 9223372036854775807L
    val float:Float   = 2147483647.0F
    val double:Double = 9223372036854775807.0
    val char:Char     = 0xFFFF.toChar
    val string:String = s"123456789"
    val bool:Boolean  = true
    val unit:Unit     = Unit
    val nullR:Null    = null
    var any:Any       = 'a'
    any               = 0
    any               = "Hello"
    var anyRef:AnyRef = "Happy"
    anyRef            = "World"

    println(s"Byte:     $byte")
    println(s"Short:    $short")
    println(s"Int:      $int")
    println(s"Long:     $long")
    println(s"Float:    $float")
    println(s"Double:   $double")
    println(s"Char:     $char")
    println(s"String:   $string")
    println(s"Boolean:  $bool")
    println(s"Unit:     $unit")
    println(s"Null:     $nullR")
    println(s"Any:      $any")
    println(s"AnyRef:   $anyRef")

  }
}
