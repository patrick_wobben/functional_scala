package examples

class FloatExamples extends Example
{
  override def example(): Unit =
  {
    println('\n')
    println("Float Examples")

    //#1 Addition
    val addition: Float = 1f + 1.0f
    println(s"#1 Addition:        1f + 1.0f = $addition")

    //#2 Subtraction
    val subtraction: Float = 1.7f - 0.01f
    println(s"#2 Subtraction:     1.7f - 0.01f = $subtraction")

    //#3 Multiplication
    val multiplication: Float = 2.1f * 000003.0f
    println(s"#3 Multiplication:  2.1f * 000003.0f = $multiplication")

    //#4 Division
    val division = 4.5f / 2
    println(s"#4 Division:        4.5f / 2 = $division")

    //#5 To Int
    val toInt = 5.5f.toInt
    println(s"#5 To Int:           5.5f.toInt = $toInt")
  }
}
