package examples

class ListExamples extends Example
{
  override def example(): Unit =
  {
    //#1 Creating a list of Strings
    val stringList: List[String] = List("Hello", "Happy", "World")
    println(stringList)

    //#2 Creating a list of Integers
    val integerList: List[Int] = List(1, 2, 3, 4)
    println(integerList)

    //#3 Creating a list of Strings 2
    val stringConsList: List[String] = "Hello" :: "Happy" :: "World" :: Nil
    println(stringConsList)

    //#4 Creating a list of Integers 2
    val integerConsList: List[Int] = 1 :: 2 :: 3 :: 4 :: Nil
    println(integerConsList)

    //#5 Creating a 2 dimensional list
    val integer2DList =
      (1 :: 2 :: 3 :: 4 :: Nil) ::
      (5 :: 6 :: 7 :: 8 :: Nil) ::
      (9 :: 0 :: 1 :: 2 :: Nil) :: Nil
    println(integer2DList)

    //#6 Head of List
    val stringHeadList: List[String] = List("Hello", "Happy", "World")
    println(stringHeadList.head)

    //#7 Head of List
    val stringTailList: List[String] = List("Hello", "Happy", "World")
    println(stringTailList.tail)

    //#8 List is empty
    val notEmptyList: List[String] = List("Hello", "Happy", "World")
    println(notEmptyList.isEmpty)
    val emptyList: List[String] = Nil
    println(emptyList.isEmpty)

    //#10 Concatenating lists
    val helloList: List[String] = List("Hello")
    val happyWorldList: List[String] = List("Happy", "World")
    val helloHappyWorldList: List[String] = List.concat(helloList, happyWorldList)
    println(helloHappyWorldList)

    //#10 Concatenating lists
    val helloList2: List[String] = List("Hello")
    val happyWorldList2: List[String] = List("Happy", "World")
    val helloHappyWorldList2: List[String] = helloList2 ::: happyWorldList2
    println(helloHappyWorldList2)

    //#11 Fill list with Filled
    val filledList = List.fill(3)("Filled")
    println(filledList)

    //#12 Tabulate list
    val squares = List.tabulate(6)(n => n * n)
    println(squares)

    //#13 Tabulate list using multiplication
    val multiplicate = List.tabulate(4, 5)(_ * _)
    println(multiplicate)

    //#14 Reverse list order
    val normalOrderList = "Hello" :: "Happy" ::"World" :: Nil
    val reverseOrderList = normalOrderList.reverse
    println(normalOrderList)
    println(reverseOrderList)
  }
}
