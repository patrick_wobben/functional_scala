package mappers

import common.types.Directory


object PathMapper
{
  def from(value : String): PathMap = new PathMap(value)

  class PathMap(value : String) extends ObjectMapper.ObjectMap
  {
    var product: String = value

    def toDomain: this.type =
    {
      product = product.substring(0, product.indexOf('/',10))
      this
    }

    //    var directoryStack : Array[Directory] = new Array[Directory]()
    val isFile: Boolean = value.length - value.lastIndexOf('.') < 4
    val isDirectory: Boolean = !isFile

    def toName: this.type =
    {
      product = product.substring(product.lastIndexOf('/') + 1)
      this
    }

    def getResult: String =
      product
  }
}
