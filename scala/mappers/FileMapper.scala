package mappers

import java.io.{File, FileOutputStream, InputStream}

object FileMapper extends ObjectMapper.ObjectMap
{
  def from(value : File): FileMap = new FileMap(value)
  def from(value : String): FileMap = new FileMap(new File(value))

  class FileMap(value : File) extends ObjectMapper.ObjectMap
  {
    def makeDirectory(): File =
    {
      val path: String = value.getAbsolutePath

      var directory: File = value
      if(directory.isFile)
        directory = new File(path.substring(0, path.lastIndexOf(File.separatorChar)))

      if(!directory.exists())
        directory.mkdir()
      directory
    }

    //noinspection AccessorLikeMethodIsUnit
    def toMoveDirectory(directoryName: String): this.type =
    {
      val temp: File = value
      if(!value.renameTo(
        new File(directoryName + '/' + value.getName)))
        println("Failed to move file")
//      if(temp.exists)
//        temp.delete()
      this
    }

    def writeAll(inputStream: InputStream): this.type =
    {
      val fileOutputStream = new FileOutputStream(value)
      val buffer: Array[Byte] = new Array[Byte](5000)
      while (inputStream.available() > 0)
      {
        inputStream.read(buffer)
        fileOutputStream.write(buffer)
      }
      this
    }
  }
}
