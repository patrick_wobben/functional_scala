package mappers

object ObjectMapper
{
  def pick[AnyType](value : AnyType): ObjectMap[AnyType] = new ObjectMap[AnyType](value)
  def from[AnyType](value : List[AnyType]): List[ObjectMap[AnyType]] =
  {
    val objectMapArray: List[ObjectMap[AnyType]] = List[ObjectMap[AnyType]]()
    value.foreach(item =>
      objectMapArray :+ new ObjectMap[AnyType](item))
    objectMapArray
  }

    class ObjectMap[AnyType](value : AnyType)
    {
      def mapEach[TKey, TValue](map : Map[TKey, TValue],
                                func : (AnyType, TKey, TValue) => Unit): this.type =
      {
        for ((k, v) <- map)
          func(value, k, v)
        this
      }

      def end[TReturn](func : AnyType => TReturn): TReturn =
        func(value)
    }
}
