package mappers

import mappers.ObjectMapper.ObjectMap

object Condition
{
  def pick[AnyType](value: AnyType): ConditionMap[AnyType] = new ConditionMap[AnyType](value)

  def isNull[AnyType](value: AnyType): Boolean =
    value == null

  def isEqual[AnyType](value: AnyType, other: AnyType): Boolean =
    value.equals(other)

  class ConditionMap[AnyType](value: AnyType) extends ObjectMap
  {
    def isNull(doFunc: => Unit): this.type =
    {
      if(value == null)
          doFunc
      this
    }

    def when(whenFunc: AnyType => Boolean, doFunc: => Unit): this.type =
    {
      if(whenFunc(value))
        doFunc
      this
    }


//    def when(whenFunc: AnyType => Boolean, doFunc: AnyType => Unit): this.type =
//    {
//      if(whenFunc(value))
//        doFunc
//      this
//    }
//    def when(whenFunc: => Boolean, doFunc: => Unit): this.type =
//    {
//      if(whenFunc())
//        doFunc
//      this
//    }

    def default(doFunc: => Unit): this.type =
    {
      doFunc
      this
    }
  }
}
