package debug

import sun.reflect.generics.tree.ReturnType

object Performance {
  def test[ReturnType](
    executeFunction: => ReturnType,
    resultFunction: (Long, ReturnType, Long) => Unit) : Unit =
      resultFunction.apply(
        System.nanoTime,
        executeFunction,
        System.nanoTime)

  def printRuntime[ReturnType](
    start: Long,
    returnvalue: ReturnType,
    end: Long): Unit =
      println("Runtime: " + ((end - start) / 1000000) + " milliseconds.")
}
