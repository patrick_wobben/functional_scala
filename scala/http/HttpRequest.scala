package http

import java.io.InputStream
import java.net.{HttpURLConnection, URL}

import common.thread.TaskPool
import mappers.{Condition, ObjectMapper}

object HttpRequest
{
  def load(path: String,
           headers: Map[String, String] = Map.empty): Result =
    ObjectMapper.pick(new URL(path)
      .openConnection()
      .asInstanceOf[HttpURLConnection])
        .mapEach(headers, (conn, key: String, value: String) =>
          conn.setRequestProperty(key, value))
        .end(conn => new Result(conn.getInputStream, conn.getErrorStream))

  class Result(input: InputStream, error:InputStream)
  {
    def onComplete( success: InputStream => Unit,
                    failure: InputStream => Unit): Unit =
      TaskPool.newTask(Condition.pick(error)
          .when(Condition.isNull, success.apply(input))
//          .when(Condition.isEqual(_, error), success.apply(input))
          .default(failure.apply(error)))
  }
}